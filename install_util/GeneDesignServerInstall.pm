package GeneDesignServerInstall;

use base 'Module::Build';
use File::Path qw(mkpath);
use File::Basename;
use IO::Socket::INET;
use File::Spec;
use IO::File;

use strict;
use warnings;

sub ACTION_install
{
  my $self = shift;
  $self->SUPER::ACTION_install;
  if ($self->_apache())
  {
    my $command = q{sudo apachectl graceful};
    my $port = $self->config_data('port');
    my $sname = $self->config_data('server_name');
    print "\nYou must restart apache for the GeneDesignServer to come online.\n";
    if ($self->y_n("Shall I try this for you?",'y'))
    {
      print "$command\n";
      system $command;
    }
    else
    {
      print "Run the command:\n\n";
      print "\t $command\n\n";
      print "When you are ready to restart apache.\n";
    }

    print "The GeneDesignServer is at $sname:$port\n\n";
  }
  else
  {
    print "No apache to be bothering with\n";
  }
  print "\n";
}

sub process_doc_files
{
  my $build = shift;
  my $files = shift;
  return unless $files;

  my $conf_dir = File::Spec->catdir($build->blib, 'genedesignserver');
  File::Path::mkpath( $conf_dir );

  foreach my $file (@{$files})
  {
    my $result = $build->copy_if_modified($file, $conf_dir) or next;
    $build->fix_shebang_line($result) unless $build->is_vmsish;
  }
  return;
}

sub _endslash
{
  my ($self, $path) = @_;
  if ($path && substr($path, -1, 1) ne q{/})
  {
    $path .= q{/};
  }
  return $path;
}

#
# Apache localization subroutines borrowed from GBrowse2
#

sub _apache_conf
{
  my $self = shift;
  my $port        = $self->config_data('port');
  my $dpath       = $self->config_data('doc_path');
  my $spath       = $dpath . 'script';
  my $apath       = $self->config_data('assets_path');
  my $sname       = $self->config_data('server_name');

  return <<END;
<VirtualHost *:$port>
  DocumentRoot $dpath
  ServerName $sname

  Alias /assets/ $apath

  <Directory "$dpath">
    Options -Indexes -MultiViews +FollowSymLinks
  </Directory>
  
  ScriptAlias /cgi-bin/ /script/
  AliasMatch ^/(.*) $spath/GeneDesignServer.pl/\$1
  
  <Directory "$spath">
    AddHandler cgi-script .pl
    Options +ExecCGI
  </Directory>
</VirtualHost>

END
}

sub _apache
{
  return -x '/usr/sbin/httpd'
    ? '/usr/sbin/httpd'
	   : -x '/usr/sbin/apache2'
       ? '/usr/sbin/apache2'
	       : -x '/usr/sbin/apache'
           ? '/usr/sbin/apache'
	           : undef;
}

sub _apache_port
{
  my @candidates = qw(80 8000 8080 8001 8008 8081 8888 8181);
  for my $port (@candidates)
  {
    my $h = IO::Socket::INET->new(LocalPort=>$port);
    return $port if $h;
  }
}

sub _apache_root
{
  my $system = $^O;
  if ($system =~ m{mswin}i)
  {  # windows system
    for
    (
    	'C:/Program Files/Apache Software Foundation/Apache2.5/',
    	'C:/Program Files/Apache Software Foundation/Apache2.4/',
    	'C:/Program Files/Apache Software Foundation/Apache2.3/',
    	'C:/Program Files/Apache Software Foundation/Apache2.2/',
    	'C:/Program Files/Apache Software Foundation/Apache2.1/',
    	'C:/Program Files/Apache Group/Apache2/',
    	'C:/Program Files/Apache Group/Apache/',
    	'C:/Apache/conf/',
    	'C:/Apache2/conf/'
    )
    {
	    return $_ if -d $_;
    }
  }
  else
  {
    for
    (
	    '/usr/local/apache2/',
	    '/usr/local/apache/', 
	    '/opt/apache2/',
	    '/opt/apache/',
	  )
    {
	    return $_ if -d $_;
    }
  }
  return;
}

sub _apache_documents
{
  local $^W = 0;

  return '/srv/genedesignserver/htdocs/' if $ENV{DEB_BUILD_ARCH};
  my $root = _apache_root();
  if ($root)
  {
    foreach ('htdocs','html')
    {
      return File::Spec->catfile($root, $_) if -e File::Spec->catfile($root, $_);
    }
  }
  for
  (
    '/var/www/html/',                  # RedHat linux
    '/var/www/htdocs/',                # Slackware linux
    '/var/www/',                       # Ubuntu/debian
    '/var/www/',                       # Ubuntu
    '/Library/WebServer/Documents/',   # MacOSX
  )
  {
    return File::Spec->catfile($_,'genedesignserver/') if -d $_;
  }
  return '/usr/local/apache/htdocs/'; # fallback
}

sub _apache_includes
{
  return '/etc/apache2/other'  if -d '/etc/apache2/other';
  return '/etc/apache2/sites-enabled/' if -d '/etc/apache2/sites-enabled/';
  return '/etc/apache2/conf-enabled/' if -d '/etc/apache2/conf-enabled/';
  return '/etc/apache2/conf.d' if -d '/etc/apache2/conf.d';
  return '/etc/apache/conf.d'  if -d '/etc/apache/conf.d';
  return '/etc/httpd/conf.d'   if -d '/etc/httpd/conf.d';
  return undef;
}

sub _apache_user
{
  for (qw(www-data www httpd apache apache2 System nobody))
  {
    return $_ if getpwnam($_);
  }
  # fallback -- user current real user
  return (getpwuid($<))[0];
}

1;