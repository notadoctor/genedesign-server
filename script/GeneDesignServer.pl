#!/usr/bin/env perl
use Mojo::Base -strict;
use File::Basename 'dirname';
use File::Spec;

use strict;
use warnings;

push @INC, join(q{/}, File::Spec->splitdir(dirname(__FILE__)), q{..}, 'lib');

# Check if Mojolicious is installed;
my $module = "Mojolicious::Commands";
(my $require_name = $module . ".pm") =~ s{::}{/}xg;
my $flag = eval
{
  require $require_name;
};
if (! $flag)
{
  die 'You do not have the Mojolicious framework installed.';
}

# Application
$ENV{MOJO_APP} ||= 'Bio::GeneDesignServer';

# Start commands
Mojolicious::Commands->start_app($ENV{MOJO_APP});

