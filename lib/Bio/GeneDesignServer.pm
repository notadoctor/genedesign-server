package Bio::GeneDesignServer;
use Mojo::Base 'Mojolicious';

use strict;
use warnings;

our $VERSION = '2.00';

=head2 startup

  This Mojolicious method will run once at server start

=cut

sub startup
{
  my $self = shift;

  my $check = eval
  {
    require Bio::GeneDesignServer::ConfigData;
  };

  $self->secrets(["Bees Sometimes Congregate Outside of Hives and Bonnets, Madam"]);

  $self->plugin('RenderFile');

  $self->defaults
  (
    {
      codon_tables_default => 'Standard',
      reverse_translation_algorithms_default => 'balanced',
      codon_juggling_algorithms_default => 'balanced',
      output_formats_default => 'genbank',
    }
  );

  if ($check)
  {
    my $gac = Bio::GeneDesignServer::ConfigData->config('google_analytics_code') || undef;
    $self->defaults(google_analytics_code => $gac);

    my $tmp = Bio::GeneDesignServer::ConfigData->config('tmp_path') || '/tmp/';
    $self->defaults(temporary_directory   => $tmp);

    my $dorg = Bio::GeneDesignServer::ConfigData->config('default_organism');
    $dorg = 'Escherichia coli' if (! length $dorg);
    $self->defaults(organisms_default     => $dorg);

    my $docpath = Bio::GeneDesignServer::ConfigData->config('doc_path') || undef;
    my $templates_path = Bio::GeneDesignServer::ConfigData->config('templates_path') || undef;
    my $assets_path = Bio::GeneDesignServer::ConfigData->config('assets_path') || undef;
    $self->defaults(doc_path => $docpath);
    $self->defaults(assets_path => $assets_path);
    push @{$self->renderer->paths}, $templates_path if ($templates_path);
  }
  else
  {
    $self->defaults(google_analytics_code => undef);
    $self->defaults(temporary_directory   => '/tmp/');
    $self->defaults(organisms_default     => 'Escherichia coli');
  }

  # Routes
  my $r = $self->routes;

  # Normal route to controller
  $r->route('/')->name('home_page')->to('Nav#functions');
  $r->route('/index')->to('Nav#functions');
  $r->route('/documentation')->to('Nav#documentation');
  $r->route('/about')->to('Nav#about');
  $r->route('/acquire')->to('Nav#acquire');

  $r->route('/RT')->to('RT#ReverseTranslation');
  $r->route('/ReverseTranslation')->to('RT#ReverseTranslation');
  $r->route('/RT/ReverseTranslation')->via(('GET'))->to('RT#ReverseTranslation');
  $r->route('/RT/ReverseTranslation')->via(('POST'))->to('RT#ReverseTranslate');
  $r->route('/RT/Results')->to('RT#Results');
  $r->route('/RT/File')->to('RT#File');

  $r->route('/CJ')->to('CJ#CodonJuggling');
  $r->route('/CodonJuggling')->to('CJ#CodonJuggling');
  $r->route('/CJ/CodonJuggling')->via(('GET'))->to('CJ#CodonJuggling');
  $r->route('/CJ/CodonJuggling')->via(('POST'))->to('CJ#CodonJuggle');
  $r->route('/CJ/Results')->to('CJ#Results');
  $r->route('/CJ/File')->to('CJ#File');

  $r->route('/SS')->to('SS#SequenceSubtraction');
  $r->route('/SequenceSubtraction')->to('SS#SequenceSubtraction');
  $r->route('/SS/SequenceSubtraction')->via(('GET'))->to('SS#SequenceSubtraction');
  $r->route('/SS/SequenceSubtraction')->via(('POST'))->to('SS#SequenceSubtract');
  $r->route('/SS/Results')->to('SS#Results');
  $r->route('/SS/File')->to('SS#File');

  $r->route('/RSCU')->to('RSCU#GeneratingRSCUTable');
  $r->route('/GenerateRSCUTable')->to('RSCU#GeneratingRSCUTable');
  $r->route('/RSCU/GenerateRSCUTable')->via(('GET'))->to('RSCU#GeneratingRSCUTable');
  $r->route('/RSCU/GenerateRSCUTable')->via(('POST'))->to('RSCU#GenerateRSCUTable');
  $r->route('/RSCU/Results')->to('RSCU#Results');
  $r->route('/RSCU/File')->to('RSCU#File');

  $r->route('/RS')->to('RS#RepeatSmashing');
  $r->route('/RepeatSmashing')->to('RS#RepeatSmashing');
  $r->route('/RS/RepeatSmashing')->via(('GET'))->to('RS#RepeatSmashing');
  $r->route('/RS/RepeatSmashing')->via(('POST'))->to('RS#RepeatSmash');
  $r->route('/RS/Results')->to('RS#Results');
  $r->route('/RS/File')->to('RS#File');
  
  return;
}

1;

__END__

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2014, GeneDesignServer developers
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

* The names of Johns Hopkins, the Joint Genome Institute, the Lawrence Berkeley
National Laboratory, the Department of Energy, and the GeneDesign developers may
not be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE DEVELOPERS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=cut