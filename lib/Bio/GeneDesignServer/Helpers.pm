package Bio::GeneDesignServer::Helpers;
require Exporter;
use base qw(Exporter);
use Cache::FileCache;
use File::Basename;
use Bio::GeneDesign;
use English '-no_match_vars';
use autodie qw(open close unlink exec);
use utf8;

use strict;
use warnings;

our $VERSION = '2.00';

our @EXPORT_OK = qw(
  purify
  get_server_url
  get_cache_handle
  create_sid
  cache_fork
  watch_cache
  pull_file_from_cache
  codon_tables
  organism_tables
);

our %EXPORT_TAGS =  (GDM => \@EXPORT_OK);

=head2 create_sid

  Use MD5 to create a random session id

=cut

sub create_sid
{
  return Digest::MD5::md5_hex(Digest::MD5::md5_hex(time().{}.rand().$$));
}

=head2 cache_fork

  Fork a command; follow its buffer

=cut

sub cache_fork
{
  my ($sid, $command, $newfile) = @_;
  #If we're the parent, prepare the url and offer a link.
  if (my $pid = fork)
  {
    return 1;
  }

  #If we're a child, launch the script, feed results to the cache
  elsif(defined $pid)
  {
    my $cache = get_cache_handle($sid);
    $cache->set($sid, [0, q{}, q{}]);
    close STDOUT;
    close STDERR;
    close STDIN;
    my $F;
    unless (open $F, "-|")
    {
      open STDERR, ">&=1";
      exec "$command";# || die "Cannot execute $command: $OS_ERROR";
    }
    my $buf = q{};
    while (<$F>)
    {
      $buf .= $_;
      $cache->set($sid, [0, $buf, $newfile]);
    }
    $cache->set($sid, [1, $buf, $newfile]);
    exit 0;
  }
 #Otherwise, uh oh
  else
  {
    return 0;
  }
}

=head2 watch_cache

  Check to see if GeneDesign is finished running

=cut

sub watch_cache
{
  my $sid = shift;
  #            running, file, buffer
  my $status = [undef, 0, undef];

  my $cache = get_cache_handle($sid);
  my $data = $cache->get($sid);

  unless($data and ref $data eq 'ARRAY')
  {
    return $status;
  }

  #If process continues, set running to 1
  #If process is done, set running to 0
  $status->[0] = $data->[0] ? 0 : 1;

  #Fill buffer
  $status->[2] = $data->[1];

  #Set file to 1 only if GD seems to have finished properly
  $status->[1] = 1 if ($status->[2] =~ /brought to you/);

  return $status;
}

=head2 pull_file_from_cache

  Rename the sid'd file to something palatable

=cut

sub pull_file_from_cache
{
  my $sid = shift;
  my $cache = get_cache_handle($sid);
  my $data = $cache->get($sid);
  my $filename = $data->[2];
  my $actualname = $filename;
  $actualname =~ s{$sid\_}{}mx;
  return ($filename, $actualname);
}

=head2 get_cache_handle

  use Cache::FileCache to follow fork processes

=cut

sub get_cache_handle
{
  my ($namespace, $username, $expire, $pinterval) = @_;
  die 'No namespace provided for get_cache_handle' if (! $namespace);
  $username = $username   || 'nobody';
  $expire = $expire       || '30 minutes';
  $pinterval = $pinterval || '4 hours';

  my $cache = Cache::FileCache->new
  (
    {
      namespace           => $namespace,
      username            => $username,
      default_expires_in  => $expire,
      auto_purge_interval => $pinterval,
    }
  );

  return $cache;
}

=head2 get_server_url

  Ask the Mojolicious controller what host and port we are running on to create
  urls without hard coding

=cut

sub get_server_url
{
  my $controller = shift;
  my $host = $controller->req->url->to_abs->host;
  my $port = $controller->req->url->to_abs->port;
  my $address = $host;
  $address .= q{:} .$port if ($port);
  return $address;
}

=head2 purify

  Remove all non nucleotide, non comma characters from a DNA input field

=cut

sub purify
{
  my ($sequence) = @_;

  my $origseq = $sequence;
  foreach my $char (qw (A T C G R Y M K S W B D H V N))
  {
    $sequence =~ s{$char}{}gmsix;
  }
  $sequence =~ s{\,}{}gmsix;

  my %disallowed = ();
  my $seqlen = length $sequence;
  foreach my $x (0 .. $seqlen - 1)
  {
    $disallowed{substr $sequence, $x, 1}++;
  }
  my @badchars = sort keys %disallowed;
  foreach my $bad (@badchars)
  {
    $origseq =~ s{$bad}{}gmsi;
  }
  return (\@badchars);
}

=head2 codon_tables

  List all of the codon tables in GeneDesign's configuration

=cut

sub codon_tables
{
  my $GD = Bio::GeneDesign->new();
  my ($rscuref, $codref) = $GD->parse_organisms();
  my %tables;
  foreach my $codorg (keys %{$codref})
  {
    my $cleancod = $codorg;
    $cleancod =~ s/\_/\ /gmix;
    $tables{$codorg} = $cleancod;
  }
  return \%tables;
}

=head2 organism_tables

  List all of the rscu tables in GeneDesign's configuration

=cut

sub organism_tables
{
  my $GD = Bio::GeneDesign->new();
  my ($rscuref, $codref) = $GD->parse_organisms();
  my %tables;
  foreach my $codorg (keys %{$rscuref})
  {
    my $cleancod = $codorg;
    $cleancod =~ s/\_/\ /gmix;
    $tables{$codorg} = $cleancod;
  }
  return \%tables;
}

1;

__END__

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2014, GeneDesignServer developers
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

* The names of Johns Hopkins, the Joint Genome Institute, the Lawrence Berkeley
National Laboratory, the Department of Energy, and the GeneDesign developers may
not be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE DEVELOPERS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=cut