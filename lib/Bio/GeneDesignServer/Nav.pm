package Bio::GeneDesignServer::Nav;
use Mojo::Base 'Mojolicious::Controller';
use utf8;

use strict;
use warnings;

our $VERSION = '2.00';

=head2 functions

  Render GeneDesign home page

=cut

sub functions
{
  my $self = shift;
  return $self->render
  (
    template => 'layouts/index',
  );
}

=head2 about

  Render about page

=cut

sub about
{
  my $self = shift;
  return $self->render
  (
    template => 'layouts/about',
  );
}

=head2 documentation

  Render docs

=cut

sub documentation
{
  my $self = shift;
  return $self->render
  (
    template => 'layouts/documentation',
  );
}

=head2 acquire

  Render information about acquisition

=cut

sub acquire
{
  my $self = shift;
  return $self->render
  (
    template => 'layouts/acquire',
  );
}

1;

__END__

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2014, GeneDesignServer developers
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

* The names of Johns Hopkins, the Joint Genome Institute, the Lawrence Berkeley
National Laboratory, the Department of Energy, and the GeneDesign developers may
not be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE DEVELOPERS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=cut