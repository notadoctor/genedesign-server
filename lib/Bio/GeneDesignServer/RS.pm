package Bio::GeneDesignServer::RS;
use Mojo::Base 'Mojolicious::Controller';
use Bio::GeneDesignServer::Helpers qw(:GDM);
use File::Basename;
use Bio::GeneDesign::ConfigData;
use English '-no_match_vars';
use utf8;

use strict;
use warnings;

our $VERSION = '2.00';

my $function_name = 'GD_Repeat_Smash';
my $module_tag    = 'RS';
my $module_name   = 'Repeat Smash';
my $module_motto  = 'banish repetitive nucleotide sequences';
my $form_route    = $module_tag . '/RepeatSmashing';
my $exec_route    = $module_tag . '/RepeatSmash';

=head2 RepeatSmashing

  Populate the form

=cut

sub RepeatSmashing
{
  my $self = shift;
  my $GD = Bio::GeneDesign->new();

  my $tables = organism_tables();
  $self->stash(organisms => $tables);
  $self->stash(orgdefault => $self->stash('organisms_default'));

  $self->stash(fileformats => $GD->export_formats());
  $self->stash(fileformatdefault => $self->stash('output_formats_default'));

  return $self->render
  (
    template => 'layouts/RepeatSmashing',
    message => undef,
    module  => $module_name,
    motto   => $module_motto,
    modtag  => $module_tag,
    baseurl => get_server_url($self),
  );
}

=head2 RepeatSmash

  Validate form input and fork the GeneDesign job

=cut

sub RepeatSmash
{
  my $self = shift;

  #Cursory validation
  my $validation = $self->validation;
  if (! $validation->has_data())
  {
    push @{ $self->session->{error_messages} }, 'No data provided!';
    return $self->redirect_to($form_route);
  }
  if ($self->req->is_limit_exceeded)
  {
    push @{ $self->session->{error_messages} }, 'The input file is too big!';
    return $self->redirect_to($form_route);
  }

  #Prepare arguments
  my $sid = create_sid();
  my $arguments = undef;

  #Output path
  my $outputpath = $self->stash('temporary_directory');
  $outputpath .= q{/} if ((substr $outputpath, -1, 1) ne q{/});
  $arguments .= ' -output ' . $outputpath;

  #Output format
  my $outputformat   = $self->param('outputformat');
  $arguments .= ' -format ' . $outputformat;

  #Input file
  my $file = $self->param('fileup');
  my $filename = $file->filename;
  my $size = $file->size;
  if (! $size)
  {
    push @{ $self->session->{error_messages} }, ' No input file provided!';
    return $self->redirect_to($form_route);
  }
  my $filepath = $outputpath . $sid . '_' . $filename;
  $file = $file->move_to($filepath);
  my ($basefile, $dirs, $suffix) = fileparse($filepath, qr/\.[^.]*/x);
  $arguments .= ' -input ' . $filepath;

  #RSCU Table or Organism
  my $rfile = $self->param('rscu');
  my $rfilename = $rfile->filename;
  my $rfilepath = $outputpath . $rfilename;
  my $rsize = $rfile->size;
  if ($rsize)
  {
    $rfile = $rfile->move_to($rfilepath);
    $arguments .= ' -rscu ' . $rfilepath;
  }
  else
  {
    my $organism = $self->param('organism');
    $organism =~ s/\ /\_/gmix;
    $arguments .= ' -organism ' . $organism;
  }

  my $script_path = Bio::GeneDesign::ConfigData->config('script_path');
  my $path = $script_path . q{/} . $function_name . '.pl';
  my $command = $path . $arguments;
  my $newfile = $basefile . q{_} . $module_tag . ".$outputformat";

  my $status = cache_fork($sid, $command, $newfile);
  $self->stash(cache => $sid);
  if ($status == 1)
  {
    my $message = "Launching GeneDesign.\n";
    $message .= 'Arguments: ';
    $message .= "$arguments\n";
    push @{$self->session->{notice_messages}}, 'Refreshing in 5 seconds';
    return $self->render
    (
      template => 'layouts/intermediate',
      message => undef,
      results => $message,
      sid     => $sid,
      module  => $module_name,
      motto   => $module_motto,
      modtag  => $module_tag,
      baseurl => get_server_url($self),
    );
  }
  elsif ($status == 0)
  {
    push @{ $self->session->{error_messages} },  'Cannot fork!';
    return $self->render
    (
      template => 'layouts/error',
      message => $command,
      results => undef,
      module  => $module_name,
      motto   => $module_motto,
      modtag  => $module_tag,
      baseurl => get_server_url($self),
    );
  }
}

=head2 Results

  Present the status of the job

=cut

sub Results
{
  my $self = shift;
  my $sid = $self->param('cache');
  if (! $sid)
  {
    push @{$self->session->{error_messages}}, 'Cannot find your session';
    return $self->redirect_to($form_route);
  }
  $self->stash(cache => $sid);
  my $status = watch_cache($sid);
  my $refresh = undef;
  if (! defined $status->[0])
  {
    push @{$self->session->{error_messages}}, 'Bad session action';
    return $self->redirect_to($form_route);
  }

  #If process continues, print out from the cache and refresh in 5
  my $console = $status->[2];
  if ($status->[0] == 1)
  {
    push @{$self->session->{notice_messages}}, 'Refreshing in 5 seconds';
    $console = "Running $function_name job $sid\n" . $console;
    $console .= "\n...continuing...\n";
    $refresh = 1;
  }
  elsif ($status->[0] == 0)
  {
    $console = "Results for $function_name job $sid\n" . $console;
    $refresh = undef;
  }
  return $self->render
  (
    template => 'layouts/results',
    message => undef,
    results => $console,
    ready   => $status->[1],
    refresh => $refresh,
    module  => $module_name,
    motto   => $module_motto,
    modtag  => $module_tag,
    baseurl => get_server_url($self),
  );
}

=head2 File

  Offer the results file

=cut

sub File
{
  my $self = shift;
  my $sid = $self->param('cache');
  if (! $sid)
  {
    push @{$self->session->{error_messages}}, 'Cannot find your session!';
    return $self->redirect_to($form_route);
  }
  $self->stash(cache => $sid);
  my ($filename, $actualname) = pull_file_from_cache($sid);
  my $fpath = $self->stash('temporary_directory');
  $fpath .= q{/} if ((substr $fpath, -1, 1) ne q{/});
  return $self->render_file
  (
    'filepath'           => $fpath . $filename,
    'filename'           => $actualname,
    'content_dispositon' => 'attachment',
  );
}

1;

__END__

=head1 COPYRIGHT AND LICENSE

Copyright (c) 2014, GeneDesignServer developers
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

* The names of Johns Hopkins, the Joint Genome Institute, the Lawrence Berkeley
National Laboratory, the Department of Energy, and the GeneDesign developers may
not be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE DEVELOPERS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=cut