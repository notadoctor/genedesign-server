#!perl -T

use Test::More tests => 7;

BEGIN
{
  use_ok( 'Bio::GeneDesignServer' )           || print "Can't use GeneDesignServer\n";
  use_ok( 'Bio::GeneDesignServer::CJ' )       || print "Can't use CJ\n";
  use_ok( 'Bio::GeneDesignServer::Helpers' )  || print "Can't use Helpers\n";
  use_ok( 'Bio::GeneDesignServer::Nav' )      || print "Can't use Nav\n";
  use_ok( 'Bio::GeneDesignServer::RSCU' )     || print "Can't use RSCU\n";
  use_ok( 'Bio::GeneDesignServer::RT' )       || print "Can't use RT\n";
  use_ok( 'Bio::GeneDesignServer::SS' )       || print "Can't use SS\n";
}
